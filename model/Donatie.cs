﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace model
{
    [Serializable]
    public class Donatie : IHasId<int>
    {
        public int Id {get;set;}
        public int IdDonator { get; set; }
        public int IdCazCaritabil { get; set; }
        public double Suma { get; set; }

        public Donatie(int id, double suma, int idD, int idC)
        {
            Id = id;
            IdDonator = idD;
            IdCazCaritabil = idC;
            Suma = suma;
        }
        public Donatie(double suma, int idD, int idC)
        {
            IdDonator = idD;
            IdCazCaritabil = idC;
            Suma = suma;
        }
        public override string ToString()
        {
            return Id + " " + IdDonator + " " + IdCazCaritabil + " " + Suma ;
        }
        public override bool Equals(object obj)
        {
            if (obj is Donatie)
            {
                Donatie st = obj as Donatie;
                return Id == st.Id;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public static bool operator ==(Donatie t1, Donatie t2)
        {
            return t1.Id == t2.Id;
        }

        public static bool operator !=(Donatie t1, Donatie t2)
        {
            return t1.Id != t2.Id;
        }

    }
}
