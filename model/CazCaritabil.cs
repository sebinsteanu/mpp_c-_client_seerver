﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace model
{
    [Serializable]
    public class CazCaritabil:IHasId<int>
    {
        public int Id { get; set; }
        public string NumeCaz { get; set; }
        public double SumaTotala { get; set; }

        public CazCaritabil(int id, string nume, double suma)
        {
            Id = id;
            NumeCaz = nume;
            SumaTotala = suma;
        }

        public override string ToString()
        {
            return Id + " " + NumeCaz + " " + SumaTotala;
        }
        public override bool Equals(object obj)
        {
            if (obj is CazCaritabil)
            {
                CazCaritabil st = obj as CazCaritabil;
                return Id == st.Id;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public static bool operator ==(CazCaritabil t1, CazCaritabil t2)
        {
            return t1.Id == t2.Id;
        }

        public static bool operator !=(CazCaritabil t1, CazCaritabil t2)
        {
            return t1.Id != t2.Id;
        }

    }
}
