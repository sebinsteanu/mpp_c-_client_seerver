﻿using System;

namespace model

{
    [Serializable]
    public class Voluntar:IHasId<int>
    {
        public Voluntar() { }
        public int Id { get; set; }

        public string Parola{ get; set; }

        public string Username { get; set; }

        public Voluntar(int id, string username, string parola)
        {
            Id = id;
            Username = username;
            Parola = parola;
        }
        public Voluntar(string username, string parola)
        {
            Username = username;
            Parola = parola;
        }


        public override string ToString()
        {
            return Id + "." + Username;
        }

        public override bool Equals(object obj)
        {
            if (obj is Voluntar)
            {
                Voluntar st = obj as Voluntar;
                return Id == st.Id;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        /*
        public static bool operator ==(Voluntar a1, Voluntar a2)
        {
            if (a1 == null || a2 == null)
                return false;
            return Equals(a1.Id, a2.Id);
        }

        public static bool operator !=(Voluntar a1, Voluntar a2)
        {
            if (a1 == null || a2 == null)
                return true;
            return !Equals (a1.Id, a2.Id);
        }
        */
    }
}
