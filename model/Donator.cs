﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace model
{
    [Serializable]
    public class Donator:IHasId<int>
    {
        public int Id { get; set; }
        public string Nume { get; set; }
        public string Adresa { get; set; }
        public string NrTel { get; set; }

        public Donator(string nume, string adresa, string nrTel)
        {
            Nume = nume;
            Adresa = adresa;
            NrTel = nrTel;
        }
        public Donator(int id, string nume, string adresa, string nrTel)
        {
            Id = id;
            Nume = nume;
            Adresa = adresa;
            NrTel = nrTel;
        }
        public override string ToString()
        {
            return Id + " " + Nume + " " + Adresa + " " + NrTel ;
        }
        public override bool Equals(object obj)
        {
            if (obj is Donator)
            {
                Donator st = obj as Donator;
                return Id == st.Id;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public static bool operator == (Donator t1, Donator t2)
        {
            return t1.Id == t2.Id;
        }

        public static bool operator !=(Donator t1, Donator t2)
        {
            return t1.Id != t2.Id;
        }

    }
}
