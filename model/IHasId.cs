﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace model
{
    public interface IHasId<T>
    {
        T Id { get; set; }
    }
}
