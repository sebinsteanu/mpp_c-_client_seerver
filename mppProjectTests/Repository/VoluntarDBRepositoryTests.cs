﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using mppProject.Model;
using mppProject.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mppProject.Repository.Tests
{
    [TestClass()]
    public class VoluntarDBRepositoryTests
    {
        private VoluntarDBRepository repo = new VoluntarDBRepository();

        [TestMethod()]
        public void saveTest()
        {
            if (repo.findOne(7) == null)
            {
                Voluntar vol = new Voluntar(7, "user", "parola");
                repo.save(vol);
                Assert.AreEqual(vol.Parola, "parola");
            }
            else
            {
                Voluntar vol = repo.findOne(7);
                Assert.AreNotEqual(vol, null);
            }
        }

        [TestMethod()]
        public void deleteTest()
        {
            if (repo.findOne(77) != null)
            {
                repo.delete(77);
                Voluntar vol = repo.findOne(77);
                Assert.AreEqual(vol, null);
            }
            else
            {
                Voluntar voln = new Voluntar(77, "username", "parola");
                repo.save(voln);

                Voluntar volnn = repo.findOne(77);
                Assert.AreEqual(volnn.Parola, "parola");

                repo.delete(77);
                Voluntar vol = repo.findOne(77);
                Assert.AreEqual(vol, default(Voluntar));
            }
        }

        [TestMethod()]
        public void updateTest()
        {
            Voluntar vol1 = repo.findOne(9);
            if (vol1 != null)
            {
                repo.update(vol1.Id, new Voluntar(9, "userr", "parola"));

                Voluntar vol2 = repo.findOne(9);
                Assert.AreEqual(vol2.Username, "userr");
            }
            else
            {
                Voluntar vol = new Voluntar(9, "user22", "parola");
                repo.save(vol);
                Assert.AreEqual(vol.Username, "user22");

                repo.update(vol.Id, new Voluntar(99, "user2", "parola2"));
                Voluntar vol2 = repo.findOne(99);
                Assert.AreEqual(vol2.Username, "user2");
            }
        }
    }
}