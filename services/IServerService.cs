﻿using model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace services
{
    public interface IServerService
    {
        void login(Voluntar voluntar, IClientService clientService);
        void makeDonation(int idCazCaritabil, Donator donator, double suma);
        IEnumerable<Donator> getFilteredDonors(String startLetters);             //filtered donors
        IEnumerable<CazCaritabil> getAllCazCaritabils();
        void logOut(Voluntar user, IClientService clientService);
    }
}
