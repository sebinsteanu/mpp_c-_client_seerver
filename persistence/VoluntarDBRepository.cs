﻿using System;
using System.Collections.Generic;
using System.Data;

using log4net;
using model;

namespace persistence
{
    public class VoluntarDBRepository : IVoluntarRepository
    {
        private static readonly ILog log = LogManager.GetLogger("VoluntarDbRepository");
        private static IDbConnection con;
        public VoluntarDBRepository()
        {
            log.Info("Creating VoluntarDbRepository");
            con = DBUtils.getConnection();
        }

        public Voluntar findOne(int id)
        {
            log.InfoFormat("Entering findOne with value {0}", id);

            using (var comm = con.CreateCommand())
            {
                comm.CommandText = "select id, username, parola from Voluntar where id=@id";
                IDbDataParameter paramId = comm.CreateParameter();
                paramId.ParameterName = "@id";
                paramId.Value = id;
                comm.Parameters.Add(paramId);

                using (var dataR = comm.ExecuteReader())
                {
                    if (dataR.Read())
                    {
                        int idV = dataR.GetInt32(0);
                        String username = dataR.GetString(1);
                        String parola = dataR.GetString(2);

                        Voluntar vol = new Voluntar(idV, username, parola);
                        log.InfoFormat("Exiting findOne with value {0}", vol);
                        return vol;
                    }
                }
            }
            log.InfoFormat("Exiting findOne with value {0}", null);
            return null;
        }
        public IEnumerable<Voluntar> findAll()
        {
            IDbConnection con = DBUtils.getConnection();
            IList<Voluntar> vols = new List<Voluntar>();
            using (var comm = con.CreateCommand())
            {
                comm.CommandText = "select id, username, parola from Voluntar";

                using (var dataR = comm.ExecuteReader())
                {
                    while (dataR.Read())
                    {
                        int idV = dataR.GetInt32(0);
                        String username = dataR.GetString(1);
                        String parola = dataR.GetString(2);

                        Voluntar vol = new Voluntar(idV, username, parola);
                        vols.Add(vol);
                    }
                }
            }
            con.Close();
            return vols;
        }
        public void save(Voluntar entity)
        {
            var con = DBUtils.getConnection();

            using (var comm = con.CreateCommand())
            {
                comm.CommandText = "insert into Voluntar (username, parola) values (@username, @parola)";
                /*var paramId = comm.CreateParameter();
                paramId.ParameterName = "@id";
                paramId.Value = entity.Id;
                comm.Parameters.Add(paramId);
                */
                var paramUser = comm.CreateParameter();
                paramUser.ParameterName = "@username";
                paramUser.Value = entity.Username;
                comm.Parameters.Add(paramUser);

                var paramParola = comm.CreateParameter();
                paramParola.ParameterName = "@parola";
                paramParola.Value = entity.Parola;
                comm.Parameters.Add(paramParola);
             
                var result = comm.ExecuteNonQuery();
                if (result == 0)
                    throw new RepositoryException("No volunteer added !");
            }
            con.Close();
        }
        public void delete(int id)
        {
            IDbConnection con = DBUtils.getConnection();
            using (var comm = con.CreateCommand())
            {
                comm.CommandText = "delete from Voluntar where id=@id";
                IDbDataParameter paramId = comm.CreateParameter();
                paramId.ParameterName = "@id";
                paramId.Value = id;
                comm.Parameters.Add(paramId);
                var dataR = comm.ExecuteNonQuery();
                if (dataR == 0)
                    throw new RepositoryException("No volunteer deleted!");
            }
            con.Close();
        }

        public void update(int idOld, Voluntar entity)
        {
            IDbConnection con = DBUtils.getConnection();
            using(var comm = con.CreateCommand()){
                comm.CommandText = "UPDATE Voluntar SET id=@id,username=@user,parola=@pass WHERE id=@idV";

                IDbDataParameter paramId = comm.CreateParameter();
                paramId.ParameterName = "@id";
                paramId.Value = entity.Id;
                comm.Parameters.Add(paramId);

                IDbDataParameter paramUser = comm.CreateParameter();
                paramUser.ParameterName = "@user";
                paramUser.Value = entity.Username;
                comm.Parameters.Add(paramUser);

                IDbDataParameter paramPass = comm.CreateParameter();
                paramPass.ParameterName = "@pass";
                paramPass.Value = entity.Parola;
                comm.Parameters.Add(paramPass);

                IDbDataParameter paramIdV = comm.CreateParameter();
                paramIdV.ParameterName = "@idV";
                paramIdV.Value = idOld;
                comm.Parameters.Add(paramIdV);

                int result = comm.ExecuteNonQuery();
                if (result == 0)
                {
                    throw new RepositoryException("No volunteer updated!");
                }
            }
            con.Close();
        }

        bool IVoluntarRepository.successfullLogin(string username, string password)
        {
            foreach (Voluntar v in this.findAll())
                if (v.Username == username && v.Parola == password)
                    return true;
            return false;
        }

        public Voluntar findBy(string username, string parola)
        {
            log.InfoFormat("Entering findBy with username value: {0}", username);

            using (var comm = con.CreateCommand())
            {
                comm.CommandText = "select username,parola from Voluntar where username=@user";
                IDbDataParameter paramId = comm.CreateParameter();
                paramId.ParameterName = "@user";
                paramId.Value = username;
                comm.Parameters.Add(paramId);

                using (var dataR = comm.ExecuteReader())
                {
                    if (dataR.Read())
                    {
                        String user = dataR.GetString(0);
                        String pass = dataR.GetString(1);

                        Voluntar vol = new Voluntar(username, parola);
                        log.InfoFormat("Exiting findOne with value {0}", vol);
                        return vol;
                    }
                }
            }
            log.InfoFormat("Exiting findOne with value {0}", null);
            return null;
        }
    }
}
