﻿using System;
using System.Collections.Generic;
using System.Data;

using model;

namespace persistence
{
    public class CazCaritabilDBRepository : IRepository<int, CazCaritabil>
    {
        private static IDbConnection con;
        public CazCaritabilDBRepository() {
            con = DBUtils.getConnection();
        }

        public void delete(int id)
        {
            using (var comm = con.CreateCommand())
            {
                comm.CommandText = "delete from CazCaritabil where id=@id";
                IDbDataParameter paramId = comm.CreateParameter();
                paramId.ParameterName = "@id";
                paramId.Value = id;
                comm.Parameters.Add(paramId);
                var dataR = comm.ExecuteNonQuery();
                if (dataR == 0)
                    throw new RepositoryException("No CazCaritabil deleted!");
            }
        }

        public IEnumerable<CazCaritabil> findAll()
        {
            IList<CazCaritabil> cazuri = new List<CazCaritabil>();
            using (var comm = con.CreateCommand())
            {
                comm.CommandText = "select id,numeCaz,sumaTotala from CazCaritabil";

                using (var dataR = comm.ExecuteReader())
                {
                    while (dataR.Read())
                    {
                        int idV = dataR.GetInt32(0);
                        String numeCaz = dataR.GetString(1);
                        double sumaT = Convert.ToDouble(dataR["sumaTotala"]);
                        CazCaritabil caz = new CazCaritabil(idV, numeCaz, sumaT);
                        cazuri.Add(caz);
                    }
                }
            }
            return cazuri;
        }

        public CazCaritabil findOne(int id)
        {
            using (var comm = con.CreateCommand())
            {
                comm.CommandText = "select id,numeCaz,sumaTotala from CazCaritabil where id=@id";
                IDbDataParameter paramId = comm.CreateParameter();
                paramId.ParameterName = "@id";
                paramId.Value = id;
                comm.Parameters.Add(paramId);

                using (var dataR = comm.ExecuteReader())
                {
                    if (dataR.Read())
                    {
                        int idV = dataR.GetInt32(0);
                        String numeCaz = dataR.GetString(1);
                        double sumaT = dataR.GetDouble(2);
                        CazCaritabil caz = new CazCaritabil(idV, numeCaz, sumaT);
                        return caz;
                    }
                }
            }
            return null;
        }

        public void save(CazCaritabil entity)
        {
            using (var comm = con.CreateCommand())
            {
                comm.CommandText = "insert into CazCaritabil  values (@id, @numeCaz, @sumaTotala)";
                var paramId = comm.CreateParameter();
                paramId.ParameterName = "@id";
                paramId.Value = entity.Id;
                comm.Parameters.Add(paramId);

                var paramNume = comm.CreateParameter();
                paramNume.ParameterName = "@numeCaz";
                paramNume.Value = entity.NumeCaz;
                comm.Parameters.Add(paramNume);

                IDbDataParameter paramSuma = comm.CreateParameter();
                paramSuma.ParameterName = "@sumaTotala";
                paramSuma.Value = entity.SumaTotala;
                comm.Parameters.Add(paramSuma);

                var result = comm.ExecuteNonQuery();
                if (result == 0)
                    throw new RepositoryException("No CazCaritabil added !");
            }
        }

        public void update(int idOld, CazCaritabil entity)
        {
            using (var comm = con.CreateCommand())
            {
                comm.CommandText = "UPDATE CazCaritabil SET numeCaz=@numeCaz,sumaTotala=@sumaTotala WHERE id=@idV";
            
                /*
                IDbDataParameter paramId = comm.CreateParameter();
                paramId.ParameterName = "@id";
                paramId.Value = entity.Id;
                comm.Parameters.Add(paramId);*/

                IDbDataParameter paramNume = comm.CreateParameter();
                paramNume.ParameterName = "@numeCaz";
                paramNume.Value = entity.NumeCaz;
                comm.Parameters.Add(paramNume);

                IDbDataParameter paramSuma = comm.CreateParameter();
                paramSuma.ParameterName = "@sumaTotala";
                paramSuma.Value = entity.SumaTotala;
                comm.Parameters.Add(paramSuma);
                
                IDbDataParameter paramIdV = comm.CreateParameter();
                paramIdV.ParameterName = "@idV";
                paramIdV.Value = idOld;
                comm.Parameters.Add(paramIdV);

                int result = comm.ExecuteNonQuery();
                if (result == 0)
                {
                    throw new RepositoryException("No CazCaritabil updated!");
                }
            }
        }
    }
}
