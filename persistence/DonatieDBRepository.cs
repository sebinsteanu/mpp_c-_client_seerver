﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using model;

namespace persistence
{
    public class DonatieDBRepository : IDonatieRepository
    {
        private DonatorDBRepository repoDonator;
        private CazCaritabilDBRepository repoCaz;
        private static IDbConnection con;

        public DonatieDBRepository(DonatorDBRepository repoDon, CazCaritabilDBRepository repoCaz)
        {
            this.repoCaz = repoCaz;
            this.repoDonator = repoDon;

            con = DBUtils.getConnection();
        }

        public IEnumerable<CazCaritabil> findAllCharityCases()
        {
            return repoCaz.findAll();
        }
        public IEnumerable<Donator> findAllDonators()
        {
            return repoDonator.findAll();
        }
        public IEnumerable<Donator> filterDonorNamesBegginingWith(string str)
        {
            return repoDonator.filterNameBegginingWith(str);
        }
        public void delete(int id)
        {
            IDbConnection con = DBUtils.getConnection();
            using (var comm = con.CreateCommand())
            {
                comm.CommandText = "delete from Donatie where id=@id";
                IDbDataParameter paramId = comm.CreateParameter();
                paramId.ParameterName = "@id";
                paramId.Value = id;
                comm.Parameters.Add(paramId);
                var dataR = comm.ExecuteNonQuery();
                if (dataR == 0)
                    throw new RepositoryException("No Donatie deleted!");
            }
            con.Close();
        }

        public void makeDonation(int idCaz, Donator donner, double suma)
        {
            string nume = donner.Nume;
            string adresa = donner.Adresa;
            string nrTel = donner.NrTel;

            //update the sum for the charity case
            CazCaritabil caz = repoCaz.findOne(idCaz);
            caz.SumaTotala += suma;
            repoCaz.update(idCaz, caz);

            //make a donation with old/new donor
            int idDonner = repoDonator.member(nume, nrTel);
            if (idDonner == 0)
            {
                repoDonator.save(new Donator(nume, adresa, nrTel));
                this.save(new Donatie( suma, repoDonator.member(nume, nrTel), idCaz));
            }
            else
            {
                 this.save(new Donatie (suma, idDonner, idCaz));
            }

        }

        public IEnumerable<Donatie> findAll()
        {
            IList<Donatie> cazuri = new List<Donatie>();
            using (var comm = con.CreateCommand())
            {
                comm.CommandText = "select id,suma,idDonator,idCazCaritabil from Donatie";

                using (var dataR = comm.ExecuteReader())
                {
                    while (dataR.Read())
                    {
                        int idV = dataR.GetInt32(0);
                        double suma = Convert.ToDouble(dataR["suma"]);
                        int idD = dataR.GetInt32(2);
                        int idC = dataR.GetInt32(3);
                        Donatie don = new Donatie(idV, suma, idD, idC);
                        cazuri.Add(don);
                    }
                }
            }
            return cazuri;
        }

        public Donatie findOne(int id)
        {
            using (var comm = con.CreateCommand())
            {
                comm.CommandText = "select id,suma,idDonator,idCazCaritabil from Donatie where id=@id";
                IDbDataParameter paramId = comm.CreateParameter();
                paramId.ParameterName = "@id";
                paramId.Value = id;
                comm.Parameters.Add(paramId);

                using (var dataR = comm.ExecuteReader())
                {
                    if (dataR.Read())
                    {
                        int idV = dataR.GetInt32(0);
                        double suma = Convert.ToDouble(dataR["suma"]);
                        int idD = dataR.GetInt32(2);
                        int idC = dataR.GetInt32(3);
                        Donatie don = new Donatie(idV, suma, idD, idC);
                        return don;
                    }
                }
            }
            return null;
        }

        public void save(Donatie entity)
        {
            using (var comm = con.CreateCommand())
            {
                comm.CommandText = "insert into Donatie (suma,idDonator, idCazCaritabil) values (@suma, @idD, @idC)";
                /*var paramId = comm.CreateParameter();
                paramId.ParameterName = "@id";
                paramId.Value = entity.Id;
                comm.Parameters.Add(paramId);
                */
                var paramSuma = comm.CreateParameter();
                paramSuma.ParameterName = "@suma";
                paramSuma.Value = entity.Suma;
                comm.Parameters.Add(paramSuma);

                IDbDataParameter paramDon = comm.CreateParameter();
                paramDon.ParameterName = "@idD";
                paramDon.Value = entity.IdDonator;
                comm.Parameters.Add(paramDon);

                IDbDataParameter paramCaz = comm.CreateParameter();
                paramCaz.ParameterName = "@idC";
                paramCaz.Value = entity.IdCazCaritabil;
                comm.Parameters.Add(paramCaz);

                var result = comm.ExecuteNonQuery();
                if (result == 0)
                    throw new RepositoryException("No Donatie added !");
            }
        }

        public void update(int idOld, Donatie entity)
        {
            using (var comm = con.CreateCommand())
            {
                comm.CommandText = "UPDATE Donatie SET id=@id,suma=@suma,idDonator=@idD,idCazCaritabil=@idC WHERE id=@idV";

                IDbDataParameter paramId = comm.CreateParameter();
                paramId.ParameterName = "@id";
                paramId.Value = entity.Id;
                comm.Parameters.Add(paramId);

                IDbDataParameter paramSuma = comm.CreateParameter();
                paramSuma.ParameterName = "@suma";
                paramSuma.Value = entity.Suma;
                comm.Parameters.Add(paramSuma);

                IDbDataParameter paramDon = comm.CreateParameter();
                paramDon.ParameterName = "@idD";
                paramDon.Value = entity.IdDonator;
                comm.Parameters.Add(paramDon);

                IDbDataParameter paramCaz = comm.CreateParameter();
                paramCaz.ParameterName = "@idC";
                paramCaz.Value = entity.IdCazCaritabil;
                comm.Parameters.Add(paramCaz);

                IDbDataParameter paramIdV = comm.CreateParameter();
                paramIdV.ParameterName = "@idV";
                paramIdV.Value = idOld;
                comm.Parameters.Add(paramIdV);

                int result = comm.ExecuteNonQuery();
                if (result == 0)
                {
                    throw new RepositoryException("No Donatie updated!");
                }
            }
        }
    }
}
