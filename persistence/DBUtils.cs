﻿using Mono.Data.Sqlite;
using System;
using System.Data;
using System.Reflection;

namespace persistence
{
    public static class DBUtils
    {
        private static IDbConnection instance = null;

        public static IDbConnection getConnection()
        {
            if (instance == null || instance.State == System.Data.ConnectionState.Closed)
            {
                instance = getNewConnection();
                instance.Open();
            }
            return instance;
        }

        private static IDbConnection getNewConnection()
        {
            return ConnectionFactory.getInstance().createConnection();
        }
    }

    public abstract class ConnectionFactory
    {
        protected ConnectionFactory()
        {
        }

        private static ConnectionFactory instance;

        public static ConnectionFactory getInstance()
        {
            if (instance == null)
            {

                Assembly assem = Assembly.GetExecutingAssembly();
                Type[] types = assem.GetTypes();
                foreach (var type in types)
                {
                    if (type.IsSubclassOf(typeof(ConnectionFactory)))
                        instance = (ConnectionFactory)Activator.CreateInstance(type);
                }
            }
            return instance;
        }

        public abstract IDbConnection createConnection();
    }

    class SqliteConnectionFactory : ConnectionFactory
    {
        //Install-Package Mono.Data.Sqlite.Portable
        //Install-Package System.Data.SQLite
        public override IDbConnection createConnection()
        {
            String connectionString = "Data Source=Teledon.db;Version=3";
            return new SqliteConnection(connectionString);
        }
    }
}
