﻿using model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace persistence
{
    public interface IDonatieRepository : IRepository<int, Donatie>
    {
        IEnumerable<CazCaritabil> findAllCharityCases();
        IEnumerable<Donator> findAllDonators();

        IEnumerable<Donator> filterDonorNamesBegginingWith(string str);
        void makeDonation(int idCaz, Donator donner, double suma);
    }
}
