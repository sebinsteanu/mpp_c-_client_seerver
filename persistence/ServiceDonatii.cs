﻿using model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace persistence
{
    class ServiceDonatii
    {
        private DonatieDBRepository repoDonatie;
        
        public ServiceDonatii() {
            this.repoDonatie = new DonatieDBRepository(new DonatorDBRepository(), new CazCaritabilDBRepository());
        }

        public IEnumerable<CazCaritabil> getAllCazCaritabils() { return repoDonatie.findAllCharityCases(); }

        public IEnumerable<Donator> getAllDonators() { return repoDonatie.findAllDonators(); }
        public IEnumerable<Donator> getFilteredDonators(string startsWith) { return repoDonatie.filterDonorNamesBegginingWith(startsWith); }

        public IEnumerable<Donatie> getAllDonatii() { return repoDonatie.findAll(); }

        public void saveDonation(int idCaz, Donator don, double suma)
        {
            repoDonatie.makeDonation(idCaz, don, suma);
        }
    }
}
