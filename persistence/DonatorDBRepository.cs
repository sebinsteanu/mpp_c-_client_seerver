﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using model;

namespace persistence
{
    public class DonatorDBRepository: IRepository<int,Donator>
    {
        private static IDbConnection con;
        public DonatorDBRepository()
        {
            con = DBUtils.getConnection();
        }

        public Donator findOne(int id)
        {
            using (var comm = con.CreateCommand())
            {
                comm.CommandText = "select id,nume,adresa,nrTelefon from Donator where id=@id";
                IDbDataParameter paramId = comm.CreateParameter();
                paramId.ParameterName = "@id";
                paramId.Value = id;
                comm.Parameters.Add(paramId);

                using (var dataR = comm.ExecuteReader())
                {
                    if (dataR.Read())
                    {
                        int idV = dataR.GetInt32(0);
                        String nume = dataR.GetString(1);
                        String adresa = dataR.GetString(2);
                        String nrTelefon = dataR["nrTelefon"].ToString();
                        Donator don = new Donator(idV, nume, adresa, nrTelefon);
                        return don;
                    }
                }
            }
            return null;
        }

        internal int member(string nume, string nrTel)
        {
            using (var comm = con.CreateCommand())
            {
                comm.CommandText = "select id from Donator where nume = @nume AND nrTelefon = @nrTel";

                IDbDataParameter paramNume = comm.CreateParameter();
                paramNume.ParameterName = "@nume";
                paramNume.Value = nume;
                comm.Parameters.Add(paramNume);

                IDbDataParameter paramNrTel = comm.CreateParameter();
                paramNrTel.ParameterName = "@nrTel";
                paramNrTel.Value = nrTel;
                comm.Parameters.Add(paramNrTel);

                using (var dataR = comm.ExecuteReader())
                {
                    if (dataR.Read())
                    {
                        int idV = dataR.GetInt32(0);
                        return idV;
                    }
                }
            }
            return 0;
        }

        public IEnumerable<Donator> findAll()
        {
            IList<Donator> dons = new List<Donator>();
            using (var comm = con.CreateCommand())
            {
                comm.CommandText = "select id,nume,adresa,nrTelefon from Donator";

                using (var dataR = comm.ExecuteReader())
                {
                    while (dataR.Read())
                    {
                        int idV = dataR.GetInt32(0);
                        String nume = dataR.GetString(1);
                        String adresa = dataR.GetString(2);
                        String nrTelefon = dataR["nrTelefon"].ToString();
                        Donator don = new Donator(nume, adresa, nrTelefon);
                        dons.Add(don);
                    }
                }
            }
            return dons;
        }

        public IEnumerable<Donator> filterNameBegginingWith(string firstLetters)
        {
            IList<Donator> dons = new List<Donator>();
            using (var comm = con.CreateCommand())
            {
                comm.CommandText = "select id,nume,adresa,nrTelefon from Donator WHERE nume LIKE @startNume";
                IDbDataParameter param = comm.CreateParameter();
                param.ParameterName = "@startNume";
                param.Value = firstLetters + "%";
                comm.Parameters.Add(param);

                using (var dataR = comm.ExecuteReader())
                {
                    while (dataR.Read())
                    {
                        int idV = dataR.GetInt32(0);
                        String nume = dataR.GetString(1);
                        String adresa = dataR.GetString(2);
                        String nrTelefon = dataR["nrTelefon"].ToString();
                        Donator don = new Donator(nume, adresa, nrTelefon);
                        dons.Add(don);
                    }
                }
            }
            return dons;
        }
        public void save(Donator entity)
        {
            using (var comm = con.CreateCommand())
            {
                comm.CommandText = "insert into Donator(nume, adresa, nrTelefon) values (@nume, @adresa, @nrTelefon)";
                /*
                var paramId = comm.CreateParameter();
                paramId.ParameterName = "@id";
                paramId.Value = entity.Id;
                comm.Parameters.Add(paramId);
                */
                var paramNume = comm.CreateParameter();
                paramNume.ParameterName = "@nume";
                paramNume.Value = entity.Nume;
                comm.Parameters.Add(paramNume);

                var paramAdresa = comm.CreateParameter();
                paramAdresa.ParameterName = "@adresa";
                paramAdresa.Value = entity.Adresa;
                comm.Parameters.Add(paramAdresa);

                IDbDataParameter paramNrTel = comm.CreateParameter();
                paramNrTel.ParameterName = "@nrTelefon";
                paramNrTel.Value = entity.NrTel;
                comm.Parameters.Add(paramNrTel);
                try
                {
                    var result = comm.ExecuteNonQuery();
                }
                catch (RepositoryException ex)
                {
                    //if (result == 0)
                        throw new RepositoryException(ex.Message  + "No Donator added !");
                }
            }
        }
        public void delete(int id)
        {
            using (var comm = con.CreateCommand())
            {
                comm.CommandText = "delete from Donator where id=@id";
                IDbDataParameter paramId = comm.CreateParameter();
                paramId.ParameterName = "@id";
                paramId.Value = id;
                comm.Parameters.Add(paramId);
                var dataR = comm.ExecuteNonQuery();
                if (dataR == 0)
                    throw new RepositoryException("No Donator deleted!");
            }
        }

        public void update(int idOld, Donator entity)
        {
            using (var comm = con.CreateCommand())
            {
                comm.CommandText = "UPDATE Donator SET id=@id,nume=@nume,adresa=@adr,nrTelefon=@nrTel WHERE id=@idV";

                IDbDataParameter paramId = comm.CreateParameter();
                paramId.ParameterName = "@id";
                paramId.Value = entity.Id;
                comm.Parameters.Add(paramId);

                IDbDataParameter paramNume = comm.CreateParameter();
                paramNume.ParameterName = "@nume";
                paramNume.Value = entity.Nume;
                comm.Parameters.Add(paramNume);

                IDbDataParameter paramAdr = comm.CreateParameter();
                paramAdr.ParameterName = "@adr";
                paramAdr.Value = entity.Adresa;
                comm.Parameters.Add(paramAdr);

                IDbDataParameter paramNr = comm.CreateParameter();
                paramNr.ParameterName = "@nrTel";
                paramNr.Value = entity.NrTel;
                comm.Parameters.Add(paramNr);

                IDbDataParameter paramIdV = comm.CreateParameter();
                paramIdV.ParameterName = "@idV";
                paramIdV.Value = idOld;
                comm.Parameters.Add(paramIdV);

                int result = comm.ExecuteNonQuery();
                if (result == 0)
                {
                    throw new RepositoryException("No volunteer updated!");
                }
            }
        }
    }
}
