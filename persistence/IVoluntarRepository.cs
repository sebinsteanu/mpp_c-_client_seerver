﻿using model;

namespace persistence
{
    public interface IVoluntarRepository:IRepository<int,Voluntar>
    {
        bool successfullLogin(string username, string password);
        Voluntar findBy(string username, string parola);
    }
}
