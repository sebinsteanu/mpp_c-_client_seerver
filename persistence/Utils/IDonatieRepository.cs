﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mppProject.Repository
{
    public interface IDonatieRepository<ID,E,Case,Donor> : IRepository<ID,E>
    {
        IEnumerable<Case> findAllCharityCases();
        IEnumerable<Donor> findAllDonators();

        IEnumerable<Donor> filterDonorNamesBegginingWith(string str);
        void makeDonation(int idCaz, string nume, string adresa, string nrTel, double suma);
    }
}
