﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mppProject.Repository.Utils
{
    interface IVoluntarRepository<ID,E>:IRepository<ID,E>
    {
        bool successfullLogin(string username, string password);
    }
}
