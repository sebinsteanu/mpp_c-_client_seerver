﻿using model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace networking
{
    public interface Response
    {
    }

    [Serializable]
    public class OkResponse : Response
    {

    }

    [Serializable]
    public class ErrorResponse : Response
    {
        private string message;

        public ErrorResponse(string message)
        {
            this.message = message;
        }

        public virtual string Message
        {
            get
            {
                return message;
            }
        }
    }

    [Serializable]
    public class GetCharityCasesResponse : Response
    {
        private IEnumerable<CazCaritabil> cases;

        public GetCharityCasesResponse(IEnumerable<CazCaritabil> cases)
        {
            this.cases = cases;
        }

        public virtual IEnumerable<CazCaritabil> Cases
        {
            get
            {
                return cases;
            }
        }
    }
    [Serializable]
    public class GetFilteredDonorsResponse : Response
    {
        private IEnumerable<Donator> _donors;

        public GetFilteredDonorsResponse(IEnumerable<Donator> donors)
        {
            this._donors = donors;
        }

        public virtual IEnumerable<Donator> Donors
        {
            get
            {
                return this._donors;
            }
        }
    }
    public interface UpdateResponse : Response
    {
    }

    [Serializable]
    public class updateDonatiiResponse: UpdateResponse
    {

    }
}
