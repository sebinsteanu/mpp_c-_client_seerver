﻿using services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using model;

namespace networking
{
    public class ClientWorker : IClientService //, Runnable
    {
        private IServerService server;
        private TcpClient connection;

        private NetworkStream stream;
        private IFormatter formatter;
        private volatile bool connected;
        public ClientWorker(IServerService server, TcpClient connection)
        {
            this.server = server;
            this.connection = connection;
            try
            {

                stream = connection.GetStream();
                formatter = new BinaryFormatter();
                connected = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }
        }

        public virtual void run()
        {
            while (connected)
            {
                try
                {
                    object request = formatter.Deserialize(stream);
                    object response = handleRequest((Request)request);
                    if (response != null)
                    {
                        sendResponse((Response)response);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.StackTrace);
                }

                try
                {
                    Thread.Sleep(1000);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.StackTrace);
                }
            }
            try
            {
                stream.Close();
                connection.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error " + e);
            }
        }
        private Response handleRequest(Request request)
        {
            Response response = null;
            if (request is LoginRequest)
            {
                Console.WriteLine("Login request ...");
                LoginRequest logReq = (LoginRequest)request;
                Voluntar vol = logReq.User;
                try
                {
                    lock (server)
                    {
                        server.login(vol, this);
                    }
                    return new OkResponse();
                }
                catch (ServiceException e)
                {
                    connected = false;
                    return new ErrorResponse(e.Message);
                }
            }
            if(request is GetCharityCasesRequest)
            {

                Console.WriteLine("Get Charity Cases Request ...");
                GetCharityCasesRequest getReq = (GetCharityCasesRequest)request;
                try
                {
                    IEnumerable<CazCaritabil> cases;
                    lock (server)
                    {
                        cases = server.getAllCazCaritabils();
                    }
                    return new GetCharityCasesResponse(cases);
                }
                catch (ServiceException e)
                {
                    return new ErrorResponse(e.Message);
                }
            }
            if (request is LogoutRequest)
            {
                Console.WriteLine("Logout request");
                LogoutRequest logReq = (LogoutRequest)request;
                Voluntar user = logReq.User;
                try
                {
                    lock (server)
                    {

                        server.logOut(user, this);
                    }
                    connected = false;
                    return new OkResponse();

                }
                catch (ServiceException e)
                {
                    return new ErrorResponse(e.Message);
                }
            }
            if(request is GetFilteredDonorsRequest)
            {
                Console.WriteLine("Filtered donors request");
                GetFilteredDonorsRequest getReq = (GetFilteredDonorsRequest)request;
                string letters = getReq.StartLetters;
                try
                {
                    IEnumerable<Donator> donors;
                    lock (server)
                    {
                        donors = server.getFilteredDonors(letters);
                    }
                    return new GetFilteredDonorsResponse(donors);
                }
                catch(ServiceException ex)
                {
                    return new ErrorResponse(ex.Message);
                }
            }

            if (request is MakeDonationRequest)
            {
                Console.WriteLine("SendMessageRequest ...");
                MakeDonationRequest senReq = (MakeDonationRequest)request;
                IList<object> boxData = senReq.Data;
                try
                {
                    lock (server)
                    {
                        server.makeDonation((int)boxData[0],(Donator) boxData[1], (double) boxData[2]);
                    }
                    return new OkResponse();
                }
                catch (ServiceException e)
                {
                    return new ErrorResponse(e.Message);
                }
            }

            return response;
        }

        private void sendResponse(Response response)
        {
            Console.WriteLine("sending response " + response);
            formatter.Serialize(stream, response);
            stream.Flush();

        }

        public void updateDonatii()
        {

            Response resp = new updateDonatiiResponse();
            try
            {
                sendResponse(resp);
            }
            catch (Exception e)
            {
                throw new ServiceException("Error on sending update to clients: " + e);
            }
        }
    }

}

