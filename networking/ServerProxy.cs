﻿using services;
using System;
using System.Collections.Generic;
using model;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Threading;
using System.Runtime.Serialization.Formatters.Binary;

namespace networking
{
    public class ServerProxy : IServerService
    {
        private string host;
        private int port;

        private IClientService client;

        private NetworkStream stream;

        private IFormatter formatter;
        private TcpClient connection;

        private Queue<Response> responses;
        private volatile bool finished;
        private EventWaitHandle _waitHandle;
        public ServerProxy(string host, int port)
        {
            this.host = host;
            this.port = port;
            responses = new Queue<Response>();
        }
        /*
        public virtual User[] getLoggedFriends(User user)
        {
            UserDTO udto = DTOUtils.getDTO(user);
            sendRequest(new GetLoggedFriendsRequest(udto));
            Response response = readResponse();
            if (response is ErrorResponse)
            {
                ErrorResponse err = (ErrorResponse)response;
                throw new ChatException(err.Message);
            }
            GetLoggedFriendsResponse resp = (GetLoggedFriendsResponse)response;
            UserDTO[] frDTO = resp.Friends;
            User[] friends = DTOUtils.getFromDTO(frDTO);
            return friends;
        }*/

        public virtual void login(Voluntar user, IClientService client)
        {
            initializeConnection();
            sendRequest(new LoginRequest(user));
            Response response = readResponse();
            if (response is OkResponse)
            {
                this.client = client;
                return;
            }
            if (response is ErrorResponse)
            {
                ErrorResponse err = (ErrorResponse)response;
                closeConnection();
                throw new ServiceException(err.Message);
            }
        }
        public IEnumerable<CazCaritabil> getAllCazCaritabils()
        {
            sendRequest(new GetCharityCasesRequest());
            Response response = readResponse();
            if (response is ErrorResponse)
            {
                ErrorResponse err = (ErrorResponse)response;
                throw new ServiceException(err.Message);
            }
            GetCharityCasesResponse resp = (GetCharityCasesResponse)response;
            IEnumerable<CazCaritabil> cases = resp.Cases;
            return cases;
        }

        public IEnumerable<Donator> getFilteredDonors(string startLetters)
        {
            sendRequest(new GetFilteredDonorsRequest(startLetters));
            Response response = readResponse();
            if(response is ErrorResponse)
            {
                ErrorResponse eroare = (ErrorResponse)response;
                throw new ServiceException(eroare.Message);
            }
            GetFilteredDonorsResponse resp = (GetFilteredDonorsResponse)response;

            return  resp.Donors;
        }
        
        public void logOut(Voluntar user, IClientService clientService)
        {
            sendRequest(new LogoutRequest(user));
            Response response = readResponse();
            closeConnection();
            if (response is ErrorResponse)
            {
                ErrorResponse err = (ErrorResponse)response;
                throw new ServiceException(err.Message);
            }
        }

        public void makeDonation(int idCazCaritabil, Donator donator, double suma)
        {
            IList<object> fieldsData = new List<object>();
            fieldsData.Add(idCazCaritabil);
            fieldsData.Add(donator);
            fieldsData.Add(suma);
            sendRequest(new MakeDonationRequest(fieldsData));
            Response resp = readResponse();
            if(resp is ErrorResponse)
            {
                ErrorResponse err = (ErrorResponse)resp;
                throw new ServiceException(err.Message);
            }
        }


        private void sendRequest(Request request)
        {
            try
            {
                formatter.Serialize(stream, request);
                stream.Flush();
            }
            catch (Exception e)
            {
                throw new ServiceException("Error sending object " + e);
            }

        }

        private Response readResponse()
        {
            Response response = null;
            try
            {
                _waitHandle.WaitOne();
                lock (responses)
                {
                    //Monitor.Wait(responses); 
                    response = responses.Dequeue();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }
            return response;
        }
        private void initializeConnection()
        {
            try
            {
                connection = new TcpClient(host, port);
                stream = connection.GetStream();
                formatter = new BinaryFormatter();
                finished = false;
                _waitHandle = new AutoResetEvent(false);
                startReader();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }
        }
        private void startReader()
        {
            Thread tw = new Thread(run);
            tw.Start();
        }
        private void closeConnection()
        {
            finished = true;
            try
            {
                stream.Close();
                //output.close();
                connection.Close();
                _waitHandle.Close();
                client = null;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }

        }
        private void handleUpdate(UpdateResponse update)
        {
            if (update is updateDonatiiResponse)
            {
                try
                {
                    client.updateDonatii();
                }
                catch(ServiceException e)
                {
                    Console.WriteLine(e.StackTrace);
                }
            }
        }
        public virtual void run()
        {
            while (!finished)
            {
                try
                {
                    object response = formatter.Deserialize(stream);
                    Console.WriteLine("response received " + response);
                    if (response is UpdateResponse)
                    {
                        handleUpdate((UpdateResponse)response);
                    }
                    else
                    {
                        lock (responses)
                        {
                            responses.Enqueue((Response)response);
                        }
                        _waitHandle.Set();
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Reading error " + e);
                }

            }
        }
    }
}
