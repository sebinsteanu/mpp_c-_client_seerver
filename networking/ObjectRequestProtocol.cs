﻿using model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace networking
{
    public interface Request
    {
    }


    [Serializable]
    public class LoginRequest : Request
    {
        private Voluntar user;

        public LoginRequest(Voluntar user)
        {
            this.user = user;
        }

        public virtual Voluntar User
        {
            get
            {
                return user;
            }
        }
    }

    [Serializable]
    public class GetCharityCasesRequest : Request
    {
    }

    [Serializable]
    public class MakeDonationRequest : Request
    {
        private IList<object> _my_data;
        public MakeDonationRequest(IList<object> data)
        {
            this._my_data = data;
        }
        public virtual IList<object> Data
        {
            get
            {
                return this._my_data;
            }
        }
    }

    [Serializable]
    public class GetFilteredDonorsRequest : Request
    {
        private string startLetter;

        public GetFilteredDonorsRequest(string startLetter)
        {
            this.startLetter = startLetter;
        }

        public virtual string StartLetters
        {
            get
            {
                return startLetter;
            }
        }
    }

    [Serializable]
    public class LogoutRequest : Request
    {
        private Voluntar user;

        public LogoutRequest(Voluntar user)
        {
            this.user = user;
        }

        public virtual Voluntar User
        {
            get
            {
                return user;
            }
        }
    }

}
