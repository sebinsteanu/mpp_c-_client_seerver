﻿using services;
using System.Windows.Forms;

using networking;

namespace mppProject
{
    class StartClient
    {
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            IServerService server = new ServerProxy ("127.0.0.1", 55555);
            ClientController ctrl = new ClientController(server);

            Application.Run(new LoginWindow(ctrl));
        }
    }
}
