﻿namespace mppProject
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAdd = new System.Windows.Forms.Button();
            this.dgvCazCaritabil = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtNume = new System.Windows.Forms.TextBox();
            this.txtAdresa = new System.Windows.Forms.TextBox();
            this.txtNrTel = new System.Windows.Forms.TextBox();
            this.txtSuma = new System.Windows.Forms.TextBox();
            this.btnLogOut = new System.Windows.Forms.Button();
            this.dgvDonatori = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCazCaritabil)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDonatori)).BeginInit();
            this.SuspendLayout();
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.25F);
            this.btnAdd.Location = new System.Drawing.Point(370, 322);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(98, 56);
            this.btnAdd.TabIndex = 0;
            this.btnAdd.Text = "doneaza";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // dgvCazCaritabil
            // 
            this.dgvCazCaritabil.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCazCaritabil.Location = new System.Drawing.Point(12, 12);
            this.dgvCazCaritabil.Name = "dgvCazCaritabil";
            this.dgvCazCaritabil.Size = new System.Drawing.Size(282, 409);
            this.dgvCazCaritabil.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.label1.Location = new System.Drawing.Point(315, 139);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Nume:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.label2.Location = new System.Drawing.Point(315, 165);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Adresa:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.label3.Location = new System.Drawing.Point(315, 236);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "Numar telefon:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.label4.Location = new System.Drawing.Point(315, 271);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(96, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "Suma donata:";
            // 
            // txtNume
            // 
            this.txtNume.Location = new System.Drawing.Point(398, 136);
            this.txtNume.Name = "txtNume";
            this.txtNume.Size = new System.Drawing.Size(124, 20);
            this.txtNume.TabIndex = 7;
            this.txtNume.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtNume_TextChanged);
            // 
            // txtAdresa
            // 
            this.txtAdresa.Location = new System.Drawing.Point(398, 165);
            this.txtAdresa.Name = "txtAdresa";
            this.txtAdresa.Size = new System.Drawing.Size(124, 20);
            this.txtAdresa.TabIndex = 8;
            // 
            // txtNrTel
            // 
            this.txtNrTel.Location = new System.Drawing.Point(422, 233);
            this.txtNrTel.Name = "txtNrTel";
            this.txtNrTel.Size = new System.Drawing.Size(124, 20);
            this.txtNrTel.TabIndex = 9;
            // 
            // txtSuma
            // 
            this.txtSuma.Location = new System.Drawing.Point(422, 268);
            this.txtSuma.Name = "txtSuma";
            this.txtSuma.Size = new System.Drawing.Size(124, 20);
            this.txtSuma.TabIndex = 10;
            // 
            // btnLogOut
            // 
            this.btnLogOut.Location = new System.Drawing.Point(471, 12);
            this.btnLogOut.Name = "btnLogOut";
            this.btnLogOut.Size = new System.Drawing.Size(75, 23);
            this.btnLogOut.TabIndex = 11;
            this.btnLogOut.Text = "Log out";
            this.btnLogOut.UseVisualStyleBackColor = true;
            this.btnLogOut.Click += new System.EventHandler(this.btnLogOut_Click);
            // 
            // dgvDonatori
            // 
            this.dgvDonatori.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDonatori.Location = new System.Drawing.Point(552, 12);
            this.dgvDonatori.Name = "dgvDonatori";
            this.dgvDonatori.Size = new System.Drawing.Size(302, 409);
            this.dgvDonatori.TabIndex = 2;
            this.dgvDonatori.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDonatori_CellClick);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(866, 433);
            this.Controls.Add(this.btnLogOut);
            this.Controls.Add(this.txtSuma);
            this.Controls.Add(this.txtNrTel);
            this.Controls.Add(this.txtAdresa);
            this.Controls.Add(this.txtNume);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgvDonatori);
            this.Controls.Add(this.dgvCazCaritabil);
            this.Controls.Add(this.btnAdd);
            this.Name = "MainWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Donatii";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainWindow_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCazCaritabil)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDonatori)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.DataGridView dgvCazCaritabil;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtNume;
        private System.Windows.Forms.TextBox txtAdresa;
        private System.Windows.Forms.TextBox txtNrTel;
        private System.Windows.Forms.TextBox txtSuma;
        private System.Windows.Forms.Button btnLogOut;
        private System.Windows.Forms.DataGridView dgvDonatori;
    }
}