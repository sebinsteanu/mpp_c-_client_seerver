﻿using services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using model;

namespace mppProject
{
    public class ClientController : IClientService
    {
        private IServerService server;
        private Voluntar currentUser;
        public event EventHandler UpdateEvent;

        public ClientController(IServerService server)
        {
            this.server = server;
        }

        public void login(string username, string password)
        {
            Voluntar user = new Voluntar(username, password);
            server.login(user, this);
            Console.WriteLine("Login succeeded ....");
            currentUser = user;
            Console.WriteLine("Current user {0}", user);
        }
        public void updateDonatii()
        {
            Console.WriteLine(".....Yeeeei.......reached the controller in UI.....");
            UpdateEvent?.Invoke(this, EventArgs.Empty);

        }

        public IEnumerable<CazCaritabil> getAllCazCaritabils()
        {
            return server.getAllCazCaritabils();
        }

        public IEnumerable<Donator> getFilteredDonators(string text)
        {
            return server.getFilteredDonors(text);
        }

        public void saveDonation(int idCaz, string nume, string adresa, string nrTel, double suma)
        {
            server.makeDonation(idCaz, new Donator(nume, adresa, nrTel), suma);
        }
        public void logOut()
        {
            server.logOut(currentUser, this);
        }
    }
}
