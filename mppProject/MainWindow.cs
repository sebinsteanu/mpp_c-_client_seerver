﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mppProject
{
    public partial class MainWindow : Form
    {
        private ClientController _ctrl;
        private BackgroundWorker bgWorker;
        public delegate void UpdateDGVCallback();


        private void loadData()
        {
            //dgvCazCaritabil.Invoke(new Action(() => dgvCazCaritabil.DataSource = this._ctrl.getAllCazCaritabils()));
            dgvCazCaritabil.DataSource = this._ctrl.getAllCazCaritabils();
        }

        public void HandleUpdateDonatii(object sender, EventArgs args)
        {
            /*
            new Thread(() =>
            {
                Thread.CurrentThread.IsBackground = true;
                loadData();
            }).Start();
            */


            //run from the UI thread
            dgvCazCaritabil.BeginInvoke(new UpdateDGVCallback(loadData), new Object[] { });


            //bgWorker.RunWorkerAsync();

            //IAsyncResult ar = this.BeginInvoke(null, null);
            //ar.AsyncWaitHandle.WaitOne();

            //loadData();

        }

        private void backgroundWorker1_RunWorkerCompleted(object sender,RunWorkerCompletedEventArgs e)
        {
            loadData();
        }
        public MainWindow(ClientController ctrl)
        {
            InitializeComponent();

            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            
            this._ctrl = ctrl;

            dgvCazCaritabil.AutoGenerateColumns = true;

            dgvDonatori.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            dgvCazCaritabil.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

            //pentru a retine la orice pas cazul caritabil selectat
            //this.btnAdd.DataBindings.Add("AccessibleDescription", dgvCazCaritabil, "id");
            
            
            //this.UpdateEvent += this._ctrl.updateDonatii();
            _ctrl.UpdateEvent += this.HandleUpdateDonatii;

            loadData();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            string nume = txtNume.Text;
            string adresa = txtAdresa.Text;
            string nrTel = txtNrTel.Text;

            double suma;
            Double.TryParse(txtSuma.Text, out suma);
            if (suma == 0)
                this._warning("Suma trebuie sa fie un numar pozitiv!");
            else
            {

                if (dgvCazCaritabil.SelectedCells.Count > 0)
                {
                    int selectedrowindex = dgvCazCaritabil.SelectedCells[0].RowIndex;
                    DataGridViewRow selectedRow = dgvCazCaritabil.Rows[selectedrowindex];

                    int id;
                    int.TryParse(Convert.ToString(selectedRow.Cells["Id"].Value), out id);
                    _ctrl.saveDonation(id, nume, adresa, nrTel, suma);
               
                    //loadData();
                }
                else
                {
                    this._warning("Selectati un caz caritabil");
                }
            }

        }
        private void _warning(String msg)
        {
            MessageBox.Show(msg, "Putina atentie va rog!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void btnLogOut_Click(object sender, EventArgs e)
        {
            this.Hide();
            try
            {
                this._ctrl.logOut();
            }
            catch (Exception ex)
            {
                this._warning(ex.Message);
            }
            this.RefToLoginForm.Show();
        }

        public Form RefToLoginForm { get; set; }

        

        private void dgvDonatori_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int selectedrowindex = dgvDonatori.SelectedCells[0].RowIndex;
            DataGridViewRow selectedRow = dgvDonatori.Rows[selectedrowindex];

            txtNume.Text = Convert.ToString(selectedRow.Cells["nume"].Value) ;
            txtAdresa.Text = Convert.ToString(selectedRow.Cells[2].Value);
            txtNrTel.Text = Convert.ToString(selectedRow.Cells[3].Value) ;
        }

        private void txtNume_TextChanged(object sender, KeyEventArgs e)
        {
            BindingSource sourceDonatori = new BindingSource();
            sourceDonatori.DataSource = this._ctrl.getFilteredDonators(txtNume.Text);
            dgvDonatori.DataSource = sourceDonatori;
        }

        private void MainWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.RefToLoginForm.Close();
        }
    }
}
