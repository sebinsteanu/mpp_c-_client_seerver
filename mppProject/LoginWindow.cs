﻿
using services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mppProject
{
    
    public partial class LoginWindow : Form
    {
        private ClientController _ctrl;
        public LoginWindow(ClientController ctrl)
        {
            InitializeComponent();
            
            this._ctrl = ctrl;
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
        }
        private void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                _ctrl.login(textBox1.Text, textBox2.Text);

                MainWindow allTheFunWindow = new MainWindow(this._ctrl);
                allTheFunWindow.RefToLoginForm = this;

                this.Visible = false;
                allTheFunWindow.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Login error! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            this.textBox1.Clear();
            this.textBox2.Clear();
        }
    }
}
