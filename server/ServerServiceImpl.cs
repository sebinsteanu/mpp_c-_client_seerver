﻿using model;
using persistence;
using services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace server
{
    class ServerServiceImpl : IServerService
    {

        private IVoluntarRepository userRepository;
        private IDonatieRepository donatiiRepo;
        private readonly IDictionary<String, IClientService> loggedClients;

        public ServerServiceImpl(IVoluntarRepository repo, IDonatieRepository donatiiRepo)
        {
            this.userRepository = repo;
            this.donatiiRepo = donatiiRepo;
            loggedClients = new Dictionary<String, IClientService>();
        }

        public void login(Voluntar user, IClientService client)
        {
            Voluntar userOk = userRepository.findBy(user.Username, user.Parola);
            if (userOk != null)
            {
                if (loggedClients.ContainsKey(user.Username))
                    throw new ServiceException("User already logged in.");
                loggedClients[user.Username] = client;
            }
            else
                throw new ServiceException("Authentication failed.");
        }

        public void makeDonation(int idCazCaritabil, Donator donator, double suma)
        {
            donatiiRepo.makeDonation(idCazCaritabil, donator, suma);
            //update all clients
            Thread.Sleep(1000);
            foreach (IClientService client in loggedClients.Values)
            {
                Task.Run(() => client.updateDonatii());
            }
        }

        public IEnumerable<Donator> getFilteredDonors(string startLetters)
        {
            return donatiiRepo.filterDonorNamesBegginingWith(startLetters);
        }

        public IEnumerable<CazCaritabil> getAllCazCaritabils()
        {
            return donatiiRepo.findAllCharityCases();
        }

        public void logOut(Voluntar user, IClientService clientService)
        {
            IClientService localClient = loggedClients[user.Username];
            if (localClient == null)
                throw new ServiceException("User " + user.Username + " is not logged in.");
            loggedClients.Remove(user.Username);
        }
    }
}
