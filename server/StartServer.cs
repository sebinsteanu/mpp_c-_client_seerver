﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using persistence;

namespace server
{
    using networking;
    using services;
    using System.Net.Sockets;
    using System.Threading;

    class StartServer
    {
        static void Main(string[] args)
        {

            //IUserRepository userRepo=new UserRepositoryDb();
            //IMessageRepository messageRepository=new MessageRepositoryDb();

            IVoluntarRepository userRepo = new VoluntarDBRepository();
            IDonatieRepository donRepo = new DonatieDBRepository(new DonatorDBRepository(), new CazCaritabilDBRepository());

            IServerService serviceImpl = new ServerServiceImpl(userRepo, donRepo);


            SerialServer server = new SerialServer("127.0.0.1", 55555, serviceImpl);
            server.Start();
            Console.WriteLine("Server started ...");
            //Console.WriteLine("Press <enter> to exit...");
            Console.ReadLine();

        }
    }

    
    public class SerialServer : ConcurrentServer
    {
        private IServerService server;
        private ClientWorker worker;
        public SerialServer(string host, int port, IServerService server) : base(host, port)
        {
            this.server = server;
            Console.WriteLine("SerialServer ...");
        }
        protected override Thread createWorker(TcpClient client)
        {
            worker = new ClientWorker(server, client);
            return new Thread(new ThreadStart(worker.run));
        }
    }
}
